function onClickLogin() {
    var userInfo = {
        'username': $('#username').val(),
        'password': $('#password').val(),
    }
            
    var accessToken = '';


    apiKey = '8d45HFhX48RqmX3poVm3ha36gAAW5EbS';
    secret = 'dBBO6uOzjek7YJmE';
    var auth = 'Basic ' + btoa(apiKey + ':' + secret);

    // var auth = 'Basic OGQ0NUhGaFg0OFJxbVgzcG9WbTNoYTM2Z0FBVzVFYlM6ZEJCTzZ1T3pqZWs3WUptRQ=='

    $.ajax({
        method: 'POST',
        url: 'https://bdo-unibank-dev.apigee.net/v1/oauth2/token',
        headers: {
            'Authorization': auth,
        },
        data: {
            'grant_type': 'password',
            'username': userInfo.username,
            'password': userInfo.password,
        },
        contentType: 'application/x-www-form-urlencoded',
        success: function(response) {
            if(response.access_token) {
                accessToken = response.access_token;
            } else {
                console.log(response);
            }
        }
    }).promise().done(function() {
        if (accessToken != '') {
            console.log(accessToken);
            localStorage.setItem('access-token', 'Bearer ' + accessToken);
            window.location.href = './landing-page.html';
        } else {
            console.log('no access token');
        }
    })
}
