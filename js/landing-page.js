var accessToken;

$(document).ready(function() {
    if(localStorage.getItem('access-token')) {
        accessToken = JSON.stringify(localStorage.getItem('access-token'));
        
    }
});

function onClickAccountBalance() {
    var acctNo = '000661354725';
    var sa = 'SA';
    var pan = '5921160000271319';

    var resultJson;

    console.log(accessToken);

    $.ajax({
        method: 'GET',
        headers: {
            'Authorization': accessToken,
        },
        url: 'https://bdo-unibank-dev.apigee.net/v1/accounts?acctNo=' + acctNo + '&acctType=' + sa +'&pan=' + pan,
        success: function(response) {
            resultJson = response;
        },
    }).promise().done(function() {
        if (resultJson != '') {
            console.log(resultJson.balanceInquiryResponse);
            var balances = localStorage.setItem('balances', JSON.stringify(resultJson.balanceInquiryResponse));
            debugger;
            window.location.href = './balance-table.html';
        }
    });
}
