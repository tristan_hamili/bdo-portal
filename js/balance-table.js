$(document).ready(function() {
    var accounts;
    if(localStorage.getItem('balances')) {
        accounts = JSON.parse(localStorage.getItem('balances'));
        console.log(accounts);
    } else {
        /* test data */
        // accounts = [{
        //     'balType': 'Current',
        //     'curAmt': {
        //         'amt': 504220.06,
        //         'curCode': 'PHP',
        //     },
        // }, {
        //     'balType': 'Avail',
        //     'curAmt': {
        //         'amt': 504220.06,
        //         'curCode': 'PHP',
        //     },
        // }];
    }

    $.each(accounts.acctBal, function(i, account){
        var template = $('#row-template');
        var amount = account.curAmt.curCode + ' ' + account.curAmt.amt;
        $(template).find('.bal-type').html(account.balType);
        $(template).find('.amount').html(amount);

        $('.form-table').append(template.html());
    });
});